##########################################################################################################################
##                      This python code is used to ingest the data into the neo4j database.                            ##
##                          The credentials can be changed for any other neo4j database                                 ##
##                                                                                                                      ##
## BE WARE THAT RUNNING THIS CODE WILL CLEAR THE DATABASE FIRST AND ONLY ADD THE DATA FROM THE CSV FILES IT LOADS FROM  ##
##########################################################################################################################
from neo4j import GraphDatabase

uri = "neo4j+s://489b0fdd.databases.neo4j.io:7687"
driver = GraphDatabase.driver(uri, auth=("neo4j", "o2qvt7fhw0-XWFveOkLVBUaNwj03AT_tBHy60ptGJ_U"))

#QUERY
queries = [
    "MATCH (n) DETACH DELETE n",
    "CREATE CONSTRAINT Ecosystem_ServiceGroup IF NOT EXISTS FOR (Ecosystem_Service:Ecosystem_Service) REQUIRE Ecosystem_Service.categories IS UNIQUE",
    "CREATE CONSTRAINT Design_Strategies IF NOT EXISTS FOR (Design_Strategies:Design_Strategies) REQUIRE Design_Strategies.strategy IS UNIQUE",
    "CREATE CONSTRAINT Building IF NOT EXISTS FOR (Building:Building) REQUIRE Building.name IS UNIQUE",
    "LOAD CSV WITH HEADERS FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_1_ES_list.csv' AS row CREATE (:Ecosystem_Service {group: row.Group, categories: row.Categories})",
    "LOAD CSV FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_2_Database_design_and_ES.csv' AS row CREATE (:Design_Strategies {Strategy: row[1]});",
    "MATCH (t:Design_Strategies {Strategy: 'Implemented design strategies (from table 1)'}) DELETE t",
    "LOAD CSV FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_3_examples_and_designs.csv' AS row CREATE (:Building {name: row[1], country: row[2], city: row[3], street: row[4], link: row[6], additional_link: row[7], photo: row[8]});",
    "MATCH (n:Building {name: 'Building name'}) DELETE n",
    "LOAD CSV FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_2_Database_design_and_ES.csv' AS row MATCH(ds:Design_Strategies), (es:Ecosystem_Service) WHERE ds.Strategy = row[1] AND es.categories = row[5] CREATE (ds)-[:Generated]->(es);",
    "LOAD CSV FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_2_Database_design_and_ES.csv' AS row MATCH(ds:Design_Strategies), (es:Ecosystem_Service) WHERE ds.Strategy = row[1] AND es.categories = row[6] CREATE (ds)-[:Generated]->(es);",
    "LOAD CSV FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_2_Database_design_and_ES.csv' AS row MATCH(ds:Design_Strategies), (es:Ecosystem_Service) WHERE ds.Strategy = row[1] AND es.categories = row[7] CREATE (ds)-[:Generated]->(es);",
    "LOAD CSV FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_2_Database_design_and_ES.csv' AS row MATCH(ds:Design_Strategies), (es:Ecosystem_Service) WHERE ds.Strategy = row[1] AND es.categories = row[8] CREATE (ds)-[:Generated]->(es);",
    "LOAD CSV FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_2_Database_design_and_ES.csv' AS row MATCH(ds:Design_Strategies), (es:Ecosystem_Service) WHERE ds.Strategy = row[1] AND es.categories = row[9] CREATE (ds)-[:Generated]->(es);",
    "LOAD CSV FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_2_Database_design_and_ES.csv' AS row MATCH(ds:Design_Strategies), (es:Ecosystem_Service) WHERE ds.Strategy = row[1] AND es.categories = row[10] CREATE (ds)-[:Generated]->(es);",
    "LOAD CSV FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_2_Database_design_and_ES.csv' AS row MATCH(ds:Design_Strategies), (es:Ecosystem_Service) WHERE ds.Strategy = row[1] AND es.categories = row[11] CREATE (ds)-[:Generated]->(es);",
    "LOAD CSV FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_2_Database_design_and_ES.csv' AS row MATCH(ds:Design_Strategies), (es:Ecosystem_Service) WHERE ds.Strategy = row[1] AND es.categories = row[12] CREATE (ds)-[:Generated]->(es);",
    "LOAD CSV FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_2_Database_design_and_ES.csv' AS row MATCH(ds:Design_Strategies), (es:Ecosystem_Service) WHERE ds.Strategy = row[1] AND es.categories = row[13] CREATE (ds)-[:Generated]->(es);",
    "LOAD CSV FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_2_Database_design_and_ES.csv' AS row MATCH(ds:Design_Strategies), (es:Ecosystem_Service) WHERE ds.Strategy = row[1] AND es.categories = row[14] CREATE (ds)-[:Generated]->(es);",
    "LOAD CSV FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_2_Database_design_and_ES.csv' AS row MATCH(ds:Design_Strategies), (es:Ecosystem_Service) WHERE ds.Strategy = row[1] AND es.categories = row[15] CREATE (ds)-[:Generated]->(es);",
    "LOAD CSV FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_2_Database_design_and_ES.csv' AS row MATCH(ds:Design_Strategies), (es:Ecosystem_Service) WHERE ds.Strategy = row[1] AND es.categories = row[16] CREATE (ds)-[:Generated]->(es);",
    "LOAD CSV FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_2_Database_design_and_ES.csv' AS row MATCH(ds:Design_Strategies), (es:Ecosystem_Service) WHERE ds.Strategy = row[1] AND es.categories = row[17] CREATE (ds)-[:Generated]->(es);",
    "LOAD CSV FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_2_Database_design_and_ES.csv' AS row MATCH(ds:Design_Strategies), (es:Ecosystem_Service) WHERE ds.Strategy = row[1] AND es.categories = row[18] CREATE (ds)-[:Generated]->(es);",
    "LOAD CSV FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_3_examples_and_designs.csv' AS row MATCH(b:Building), (ds:Design_Strategies) WHERE b.name = row[1] AND ds.Strategy = row[9] CREATE (b)-[:Implemented]->(ds);",
    "LOAD CSV FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_3_examples_and_designs.csv' AS row MATCH(b:Building), (ds:Design_Strategies) WHERE b.name = row[1] AND ds.Strategy = row[10] CREATE (b)-[:Implemented]->(ds);",
    "LOAD CSV FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_3_examples_and_designs.csv' AS row MATCH(b:Building), (ds:Design_Strategies) WHERE b.name = row[1] AND ds.Strategy = row[11] CREATE (b)-[:Implemented]->(ds);",
    "LOAD CSV FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_3_examples_and_designs.csv' AS row MATCH(b:Building), (ds:Design_Strategies) WHERE b.name = row[1] AND ds.Strategy = row[12] CREATE (b)-[:Implemented]->(ds);",
    "LOAD CSV FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_3_examples_and_designs.csv' AS row MATCH(b:Building), (ds:Design_Strategies) WHERE b.name = row[1] AND ds.Strategy = row[13] CREATE (b)-[:Implemented]->(ds);",
    "LOAD CSV FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_3_examples_and_designs.csv' AS row MATCH(b:Building), (ds:Design_Strategies) WHERE b.name = row[1] AND ds.Strategy = row[14] CREATE (b)-[:Implemented]->(ds);",
    "LOAD CSV FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_3_examples_and_designs.csv' AS row MATCH(b:Building), (ds:Design_Strategies) WHERE b.name = row[1] AND ds.Strategy = row[15] CREATE (b)-[:Implemented]->(ds);",
    "LOAD CSV FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_3_examples_and_designs.csv' AS row MATCH(b:Building), (ds:Design_Strategies) WHERE b.name = row[1] AND ds.Strategy = row[16] CREATE (b)-[:Implemented]->(ds);",
    "LOAD CSV FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_3_examples_and_designs.csv' AS row MATCH(b:Building), (ds:Design_Strategies) WHERE b.name = row[1] AND ds.Strategy = row[17] CREATE (b)-[:Implemented]->(ds);",
    "LOAD CSV FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_3_examples_and_designs.csv' AS row MATCH(b:Building), (ds:Design_Strategies) WHERE b.name = row[1] AND ds.Strategy = row[18] CREATE (b)-[:Implemented]->(ds);",
    "LOAD CSV FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_3_examples_and_designs.csv' AS row MATCH(b:Building), (ds:Design_Strategies) WHERE b.name = row[1] AND ds.Strategy = row[19] CREATE (b)-[:Implemented]->(ds);",
    "LOAD CSV FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_3_examples_and_designs.csv' AS row MATCH(b:Building), (ds:Design_Strategies) WHERE b.name = row[1] AND ds.Strategy = row[20] CREATE (b)-[:Implemented]->(ds);",
    "LOAD CSV FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_3_examples_and_designs.csv' AS row MATCH(b:Building), (ds:Design_Strategies) WHERE b.name = row[1] AND ds.Strategy = row[21] CREATE (b)-[:Implemented]->(ds);",
    "LOAD CSV FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_3_examples_and_designs.csv' AS row MATCH(b:Building), (ds:Design_Strategies) WHERE b.name = row[1] AND ds.Strategy = row[22] CREATE (b)-[:Implemented]->(ds);",
    "LOAD CSV FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_3_examples_and_designs.csv' AS row MATCH(b:Building), (ds:Design_Strategies) WHERE b.name = row[1] AND ds.Strategy = row[23] CREATE (b)-[:Implemented]->(ds);",
    "LOAD CSV FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_3_examples_and_designs.csv' AS row MATCH(b:Building), (ds:Design_Strategies) WHERE b.name = row[1] AND ds.Strategy = row[24] CREATE (b)-[:Implemented]->(ds);",
    "LOAD CSV FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_3_examples_and_designs.csv' AS row MATCH(b:Building), (ds:Design_Strategies) WHERE b.name = row[1] AND ds.Strategy = row[25] CREATE (b)-[:Implemented]->(ds);",
    "LOAD CSV FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_3_examples_and_designs.csv' AS row MATCH(b:Building), (ds:Design_Strategies) WHERE b.name = row[1] AND ds.Strategy = row[26] CREATE (b)-[:Implemented]->(ds);",
    "LOAD CSV FROM 'https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/raw/main/TABLE_3_examples_and_designs.csv' AS row MATCH(b:Building), (ds:Design_Strategies) WHERE b.name = row[1] AND ds.Strategy = row[27] CREATE (b)-[:Implemented]->(ds);",
]

with driver.session() as session:
    for query in queries:
        session.run(query)