import React, { useRef, useEffect } from 'react';
import { useMap, useMapEvent, TileLayer } from 'react-leaflet';
import L from 'leaflet';
import 'leaflet-geotiff';

const GeoTIFFLayer = ({ url, options }) => {
  const map = useMap();
  const layerRef = useRef(null);

  useEffect(() => {
    const layer = L.leafletGeotiff(url, options).addTo(map);
    layerRef.current = layer;

    return () => {
      map.removeLayer(layer);
    };
  }, [map, url, options]);

  useMapEvent('moveend', () => {
    if (layerRef.current) {
      layerRef.current.bringToFront();
    }
  });

  return null;
};

export default GeoTIFFLayer;
