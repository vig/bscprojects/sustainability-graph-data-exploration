import { useState, useEffect } from 'react';
import axios from 'axios';
import BuildingData from './getBuildingData';

    const Markers = () => {
      const [buildingData, setBuildingData] = useState(BuildingData());
      console.log(BuildingData())
      useEffect(() => {
        const fetchCoordinates = async (address) => {
          try {
            const response = await axios.get(`https://nominatim.openstreetmap.org/search?q=${address}&format=json&limit=1`);
            if (response.data.length) {
              const { lat, lon } = response.data[0];
              return { lat, lon };
            } else {
              throw new Error('Could not find address');
            }
          } catch (error) {
            console.error(error);
            throw new Error('Failed to fetch coordinates');
          }
        };
    
        const updateBuildingData = async () => {
          try {
            const updatedBuildingData = await Promise.all(
              buildingData.map(async (item) => {
                const address = item.address;
                if (!address) {
                  // skip item with empty address
                  return item;
                }
                const coordinates = await fetchCoordinates(address);
                console.log(coordinates)
                return {
                  ...item,
                  coordinates: coordinates,
                };
              })
            );
            setBuildingData(updatedBuildingData);
          } catch (error) {
            console.error(error);
          }
        };
    
        updateBuildingData();
      }, []);
    
      return (buildingData)
    }
      export default Markers
