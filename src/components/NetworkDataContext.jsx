import { createContext } from 'react';

export const NetworkDataContext = createContext();
