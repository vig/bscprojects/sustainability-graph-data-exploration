# Sustainable Building Design Graph Data Exploration

## Description
This project revolves around knowledge graph visualization on sustainable building design. The application has been developed for architects and building designers to easily determine the feasibility of implementing different sustainable design strategies into their future projects. In addition, the dataset on this topic can be explored and appended using the application.
A detailed description of the project can be found in Bsc_Thesis_Charles_Klijnman_5995272.pdf.


## Installation
1. Run `npm install` to install the dependencies.
1. Update `config.jsx` and put the Neo4j credentials and database name.
2. Run the application using `npm start` and view the application in your browser with URL: http://localhost:3000. You can login with the username and password provided in `config.jsx`.

### Importing your own database
If you have no access to a Neo4j database with the corresponding dataset, you can set up your own local database:

1. Install Neo4j locally (https://console.neo4j.io/), and use the [`DataIngestion.py`](https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/blob/main/DataIngestion.py) script to import csv data in this report into a clean Neo4j database. Do not forget to fill in your own Neo4j Credentials in the `DataIngestion.py` script.
2. Uncomment line 12 from `scenes/information/index.jsx`, run the application, and go to the "Tool information page" *once*. The application will now add the latitude and longitude as points to the building nodes, which can take several minutes (don't leave the page). You can check the developer console for the status.
3. Comment line 12 again.



## Usage
For usage information, read the [user manual](https://git.science.uu.nl/vig/bscprojects/sustainability-graph-data-exploration/-/blob/main/User_Manual.pdf).

## Visuals
Some screenshots of the application:

Knowledge Graph
![alt text](/Screenshots/Graphview.png)

Map
![alt text](/Screenshots/Mapview.png)

Multi-view
![alt text](/Screenshots/Multiview.png)

Form
![alt text](/Screenshots/Forms.png)

Table
![alt text](/Screenshots/Tables.png)

Tool information
![alt text](/Screenshots/Tool_information.png)

## Authors and acknowledgment
This tool has been developed by Charles Klijnman in collaboration with Katharina Hecht. Supervised by Michael Behrisch.
completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
