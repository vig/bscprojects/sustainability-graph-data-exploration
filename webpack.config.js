// const path = require("path");
// const HtmlWebpackPlugin = require("html-webpack-plugin");
// const { HotModuleReplacementPlugin } = require("webpack");
// const fs = require("fs");

// module.exports = (_, argsv) => {
//   const isDevelopment = argsv.mode === "development";
//   const sslCertPath = path.resolve(__dirname, "ssl", "server.crt");
//   const sslKeyPath = path.resolve(__dirname, "ssl", "server.key");

//   const devServerConfig = {
//     https: {
//       cert: fs.readFileSync(sslCertPath),
//       key: fs.readFileSync(sslKeyPath),
//     },
//     hot: true,
//     open: true,
//     compress: true,
//     port: 3000,
//   };

//   return {
//     entry: "./src/index.js",
//     output: {
//       path: path.resolve(__dirname, "dist"),
//       filename: "bundle.js",
//     },
//     module: {
//       rules: [
//         {
//           test: /\.(js|jsx)$/,
//           exclude: /node_modules/,
//           use: {
//             loader: "babel-loader",
//           },
//         },
//       ],
//     },
//     plugins: [
//       new HtmlWebpackPlugin({
//         template: path.resolve(__dirname, "public", "index.html"),
//       }),
//       new HotModuleReplacementPlugin(),
//     ],
//     devtool: isDevelopment ? "eval-source-map" : "source-map",
//     devServer: devServerConfig,
//   };
// };
