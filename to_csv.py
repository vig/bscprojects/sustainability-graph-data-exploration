import pandas as pd

data = pd.read_excel('Dataset.xlsx', sheet_name=None)

# loop through the dictionary and save csv
for sheet_name, df in data.items():
    df.to_csv(f'{sheet_name}.csv')